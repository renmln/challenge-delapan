# BCR API

1. API Documentation= https://challenge-delapan.herokuapp.com/documentation/
2. List Car (GET) = https://challenge-delapan.herokuapp.com/v1/cars
3. Get Car (GET) = https://challenge-delapan.herokuapp.com/v1/cars/:id
4. Create Car (POST) = https://challenge-delapan.herokuapp.com/v1/cars
5. Update Car (PUT) = https://challenge-delapan.herokuapp.com/v1/cars/:id
6. Delete Car (DELETE) = https://challenge-delapan.herokuapp.com/v1/cars/:id
7. Rent Car (POST) = https://challenge-delapan.herokuapp.com/v1/cars/:id/rent
8. Login (POST) = https://challenge-delapan.herokuapp.com/v1/auth/login
9. Register (POST) = https://challenge-delapan.herokuapp.com/v1/auth/register
10. Who Am I (GET) = https://challenge-delapan.herokuapp.com/v1/auth/whoami

Rena Muliani - FSW9